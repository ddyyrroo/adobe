### Prerequisites
* Node >= 6

### Setup
```bash
$ git clone git@gitlab.com:ddyyrroo/adobe.git
$ cd adobe
$ npm install
```

### Running the Project
You can run the project after running through "Setup" by running:
```bash
$ npm start
```
This will run each excercise, printing the solution, the problem, and the time elapsed during the execution of the solution.

### Running Tests
The tests can be run with:
```bash
$ npm test
```
### Technologies Used
* TypeScript 2.5 / Node
* Mocha (Test Runner)
* Chai (Assertions)

### Problems
#### 11. Largest product in a grid
###### Sample Output
> 11. Largest product in a grid
> https://projecteuler.net/problem=11
> Answer: 70600674
> Elapsed time: 1.974ms

###### Why this Problem?
Good problem for getting the testing / compilation of this project setup.

###### Process
I first defined a test for parsing the input into a 2d array. I then followed TDD for each direction. I wrote a test
for a direction, then implemented it.

###### Time
This solution took ~20m

#### 36. Double-base palindromes
###### Sample Output
> 36. Double-base palindromes
> https://projecteuler.net/problem=36
> Answer:  872187
> Elapsed time: 742.048ms

###### Why this Problem?
I was curious about ES6's new binary number functionality. In the end, I used strings instead.

###### Process
This problem's process was pretty simple. I wrote the tests for the `isPalindrome` and `isNumberPalindromeInBase2AndBase10`
functions, then I started writing the implementations. I experimented with ES6's binary number type, but in the end
I converted both the base10 and base2 numbers to strings. I also rewrote the `isPalindrome` function to be more
efficient. The first implementation compared the original string to its reverse by converting the string to an array,
reversing it, and joining the array to make it a string again.

###### Time
This solution took ~30m

#### 54. Poker hands
###### Sample Output
> 54. Poker hands
> https://projecteuler.net/problem=54
> Answer: 376
> Elapsed time: 80.984ms

###### Why this Problem?
I enjoy playing poker

###### Process
At the top of src/54.ts, I defined the types I thought I would use. Most of the time was spent designing
how to handle the comparisons of hands where both players had the same hand. I ended up rewriting the hand comparison
types once to eliminate some unneeded code.

###### Time
This solution took the most time. I spent ~2h15ma on it.