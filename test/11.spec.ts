import { expect } from "chai";
import { largestProduct, parseGrid } from "../src/11";

const HORIZONTAL_TEST =
`0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 
0 0 0 2 2 2 2 0 0 0 
0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 
0 0 1 1 1 1 0 0 0 0 
0 0 0 0 0 0 0 0 0 0`;

const VERTICAL_TEST =
`0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 3
0 0 0 0 0 0 0 0 0 3
0 0 0 0 0 0 0 0 0 3
0 8 0 0 0 0 0 0 0 3
0 8 0 0 0 0 0 0 0 3
0 8 0 0 0 0 0 0 0 3
0 8 0 0 0 0 0 0 0 3
0 0 0 0 0 0 0 0 0 3
0 0 0 0 0 0 0 0 0 3`;

const DIAGONAL =
`0 0 9 0 0 0 0 0 0 0
0 9 0 0 0 0 0 0 0 0
9 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 8 0 0 0 0
0 0 0 0 8 0 0 0 0 0
0 0 0 8 0 0 0 0 0 0
0 0 8 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0`;


const ALL_DIRECTIONS =
`0 0 9 0 0 0 0 0 0 0
0 9 0 0 0 0 0 0 0 3
9 0 0 0 0 0 0 0 0 3
0 9 0 0 0 0 0 0 0 3
0 9 0 2 2 2 2 0 0 3
0 9 0 0 8 0 0 0 0 3
0 9 0 8 0 0 0 0 0 3
0 0 8 0 0 0 0 0 0 3
0 0 1 1 1 1 0 0 0 3
0 0 0 0 0 0 0 0 0 3`;


describe("11. Largest product in a grid", () => {
    it("should parse grid", () => {
        const parsedGrid = parseGrid("08 08\n08 08");
        expect(parsedGrid).to.eql([[ 8, 8 ], [ 8, 8 ]])
    });

    it("should calculate largest horizontal product in grid", () => {
        const parsedGrid = parseGrid(HORIZONTAL_TEST);
        expect(largestProduct(parsedGrid)).to.equal(16);
    });

    it("should calculate largest vertical product in grid", () => {
        const parsedGrid = parseGrid(VERTICAL_TEST);
        expect(largestProduct(parsedGrid)).to.equal(4096);
    });

    it("should calculate largest upward diagonal product in grid", () => {
        const parsedGrid = parseGrid(DIAGONAL);
        expect(largestProduct(parsedGrid)).to.equal(4096);
    });

    it("should calculate largest downward diagonal product in grid", () => {
        const parsedGrid = parseGrid(DIAGONAL);
        expect(largestProduct(parsedGrid)).to.equal(4096);
    });

    it("should calculate largest product in grid", () => {
        const parsedGrid = parseGrid(ALL_DIRECTIONS);
        expect(largestProduct(parsedGrid)).to.equal(6561);
    });
});

