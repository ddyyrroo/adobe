import { isPalindromeRedux, isNumberPalindromeInBase2AndBase10 } from "../src/36";
import { assert } from "chai";

describe("36. Double-base palindromes", () => {
    it("should validate even length palindrome", () => {
        const valid = "ABCCBA";
        assert(isPalindromeRedux(valid));

        const invalid = "BBCCBA";
        assert(!isPalindromeRedux(invalid));
    });

    it("should validate odd length palindrome", () => {
        const valid = "ABCBA";
        assert(isPalindromeRedux(valid));

        const invalid = "BBCBA";
        assert(!isPalindromeRedux(invalid));
    });

    it("should validate empty string as palindrome", () => {
        const valid = "";
        assert(isPalindromeRedux(valid));
    });

    it("should validate string with length of one as palindrome", () => {
        const valid = "A";
        assert(isPalindromeRedux(valid));
    });

    it("should determine 585 is palindrome in base2 / base10", () => {
        assert(isNumberPalindromeInBase2AndBase10(585));
    });

    it("should determine 99 is palindrome in base2 / base10", () => {
        assert(isNumberPalindromeInBase2AndBase10(99));
    });

    it("should determine 33 is palindrome in base2 / base10", () => {
        assert(isNumberPalindromeInBase2AndBase10(33));
    });
});
