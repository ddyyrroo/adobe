import { expect, assert } from "chai";
import {
    Suit, parseCard, parseHand, parseHands, isStraight, isFlush,
    getMatches, doesPlayerOneWin
} from "../src/54";

describe("54. Poker hands", () => {
    describe("Parsing", () => {
        it("should parse cards correctly", () => {
            const sixOfHearts = parseCard("6H");
            expect(sixOfHearts).to.eql({value: 6, suit: Suit.Hearts});

            const twoOfSpades = parseCard("2S");
            expect(twoOfSpades).to.eql({value: 2, suit: Suit.Spades});

            const jackOfClubs = parseCard("JC");
            expect(jackOfClubs).to.eql({value: 11, suit: Suit.Clubs});

            const aceOfDiamonds = parseCard("AD");
            expect(aceOfDiamonds).to.eql({value: 14, suit: Suit.Diamonds});
        });

        it("should throw on incorrect input format", () => {
            expect(() => { parseCard("CJ"); }).to.throw("Bad Input: suit J is invalid")
        });

        it("should parse hand of cards", () => {
            const hand = "8C TS KC 9H 4S";
            const parsedHand = parseHand(hand);

            const [a, b, c, d, e] = parsedHand;

            expect(a).to.eql({value: 8, suit: Suit.Clubs});
            expect(b).to.eql({value: 10, suit: Suit.Spades});
            expect(c).to.eql({value: 13, suit: Suit.Clubs});
            expect(d).to.eql({value: 9, suit: Suit.Hearts});
            expect(e).to.eql({value: 4, suit: Suit.Spades});
        });

        it("should parse hands of cards for both players", () => {
            const hands = "8C TS KC 9H 4S 8C TS KC 9H 4S";
            const [handOne, handTwo] = parseHands(hands);

            const [a, b, c, d, e] = handOne;
            const [f, g, h, i, j] = handTwo;

            expect(a).to.eql({value: 8, suit: Suit.Clubs});
            expect(b).to.eql({value: 10, suit: Suit.Spades});
            expect(c).to.eql({value: 13, suit: Suit.Clubs});
            expect(d).to.eql({value: 9, suit: Suit.Hearts});
            expect(e).to.eql({value: 4, suit: Suit.Spades});

            expect(f).to.eql({value: 8, suit: Suit.Clubs});
            expect(g).to.eql({value: 10, suit: Suit.Spades});
            expect(h).to.eql({value: 13, suit: Suit.Clubs});
            expect(i).to.eql({value: 9, suit: Suit.Hearts});
            expect(j).to.eql({value: 4, suit: Suit.Spades});
        });
    });

    describe("Rankings", () => {
        it("should identify a straight", () => {
            const straight = isStraight(parseHand("2H 3H 4H 5H 6H"));
            assert(straight);

            const straight2 = isStraight(parseHand("8H 9H TH JH QH"));
            assert(straight2);

            const notAStraight = isStraight(parseHand("2H 4H 6H 8H TH"))
            assert(!notAStraight);
        });

        it("should identify a flush", () => {
            const flush = isFlush(parseHand("2H 3H 4H 5H 6H"));
            assert(flush);

            const flush2 = isFlush(parseHand("2H KH 4H AH 6H"));
            assert(flush2);

            const notAFlush = isFlush(parseHand("2H KH 4S AH 6H"));
            assert(!notAFlush);
        });

        it("should identify matches", () => {
            const matches = getMatches(parseHand("2H 2C 3H 4H 5H"));
            expect(matches.length).to.be.eql(1);
            expect(matches[0]).to.eql({value: 2, count: 2});

            const matches2 = getMatches(parseHand("2H 2C 3H 3D 5H"));
            expect(matches2.length).to.be.eql(2);
            expect(matches2[0]).to.eql({value: 2, count: 2});
            expect(matches2[1]).to.eql({value: 3, count: 2});

            const matches3 = getMatches(parseHand("2H 3C 4H 6D 5H"));
            expect(matches3.length).to.be.eql(0);

            const matches4 = getMatches(parseHand("2H 2C 3H 3D 3D"));
            expect(matches4.length).to.be.eql(2);
            expect(matches4[0]).to.eql({value: 2, count: 2});
            expect(matches4[1]).to.eql({value: 3, count: 3});

            const matches5 = getMatches(parseHand("2H 2C 2D 2S 3D"));
            expect(matches5.length).to.be.eql(1);
            expect(matches5[0]).to.eql({value: 2, count: 4});
        });

        it("Pair > HighCard", () => {
            const [a, b] = parseHands("2H 2D 4H 5H 6H 2S 3S 4S 5S 8D");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("TwoPair > Pair", () => {
            const [a, b] = parseHands("2H 2C 4H 4C 6H 2S 2D 4S 5S 8S");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("ThreeOfAKind > TwoPair", () => {
            const [a, b] = parseHands("2H 2C 2S 5H 6H 2S 2D 4S 4H 8S");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("Straight > ThreeOfAKind", () => {
            const [a, b] = parseHands("2H 3H 4C 5D 6S 2H 2C 2S 5H 6H");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("Flush > Straight", () => {
            const [a, b] = parseHands("2H 3H 5H 7H 9H 2H 3H 4C 5D 6S");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("FullHouse > Flush", () => {
            const [a, b] = parseHands("2C 2D 2S 3C 3D 2H 3H 5H 7H 9H");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("FourOfAKind > FullHouse", () => {
            const [a, b] = parseHands("8H 8D 8S 8C 2H 2C 2D 2S 3C 3D");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("StraightFlush > FourOfAKind", () => {
            const [a, b] = parseHands("2C 3C 4C 5C 6C 8H 8D 8S 8C 2H");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("RoyalFlush > StraightFlush", () => {
            const [a, b] = parseHands("TH JH QH KH AH 2S 3S 4S 5S 6S");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });
    });

    describe("Rankings - Tie Breakers", () => {
        it("FourOfAKind vs FourOfAKind", () => {
            const [a, b] = parseHands("8H 8D 8S 8C AH 2C 2D 2S 2H 3D");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("OnePair vs OnePair", () => {
            const [a, b] = parseHands("8H 8D 7S 9C AH 2C AD 7S 8C 8S");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("HighCard vs HighCard", () => {
            const [a, b] = parseHands("2H 4D 7S 8C JH JC TD 2S 2H 3D");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(!playerOneDidWin);
        });

        it("StraightFlush vs StraightFlush", () => {
            const [a, b] = parseHands("3H 4H 5H 6H 7H 2S 3S 4S 5S 6S");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });

        it("FullHouse vs FullHouse", () => {
            const [a, b] = parseHands("2H 2D 3S 3C 3H AC AD 2S 2H 2D");
            const playerOneDidWin = doesPlayerOneWin(a, b);
            assert(playerOneDidWin);
        });
    });
});