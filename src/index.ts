import { run as runGrid } from "./11";
import { run as runPalindrome } from "./36";
import { run as runPoker } from "./54";

runGrid();
runPalindrome();
runPoker();
