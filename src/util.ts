const timeLabel = "Elapsed time";

export default function runTest(label: string, url: string, testFn: () => void): void {
    console.log(label);
    console.log(url);

    console.time(timeLabel);
    testFn();
    console.timeEnd(timeLabel);

    console.log("");
}