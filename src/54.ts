import { readFileSync } from "fs";
import runSolution from "./util";

const POKER_FILE_PATH = "./src/data/poker.txt";
const ACE_VALUE = 14;

export enum Suit {
    Hearts, Clubs, Diamonds, Spades
}

// Ranking of Poker Hands in order
export enum HandRanking {
    HighCard, OnePair, TwoPair, ThreeOfAKind, Straight, Flush, FullHouse, FourOfAKind, StraitFlush, RoyalFlush
}

export type Card = {
    suit: Suit;
    value: number;
}

type DeterminingCardRanks = HandRanking.Straight | HandRanking.StraitFlush | HandRanking.Flush
                         | HandRanking.ThreeOfAKind | HandRanking.FourOfAKind | HandRanking.FullHouse;

// HandRankings where ties can be determined with one card
class DeterminingCard {
    constructor(public rank: DeterminingCardRanks,
                public determiningCard: number) {}
}


// HandRankings where ties must be determined with a high card or the highest paired
// card, and the remaining supporting cards. For example, If both players get a pair
// of the same value, the winner will be determined by who has the highest card not
// in the pair.
class SupportingCard {
    constructor(public rank: HandRanking.HighCard | HandRanking.OnePair | HandRanking.TwoPair,
                public determiningCard: number,
                public supportingCards: number[]) {}
}

class NoContestComparison {
    constructor(public rank = HandRanking.RoyalFlush) {}
}

type HandComparison = NoContestComparison | DeterminingCard | SupportingCard;

export function parseValue(value: string): number {
    const parsed = parseInt(value);

    if (isNaN(parsed)) {
        switch (value) {
            case "T": return 10;
            case "J": return 11;
            case "Q": return 12;
            case "K": return 13;
            case "A": return 14;
            default: throw new Error("Bad Input: value ${value} is invalid");
        }
    } else {
        return parsed;
    }
}

export function parseSuit(suit: string): Suit {
    switch (suit) {
        case "H": return Suit.Hearts;
        case "C": return Suit.Clubs;
        case "D": return Suit.Diamonds;
        case "S": return Suit.Spades;
        default: throw new Error(`Bad Input: suit ${suit} is invalid`);
    }
}

export function parseCard(card: string): Card {
    const [value, suit] = card.split("");
    return { suit: parseSuit(suit), value: parseValue(value)};
}

export function parseHand(hand: string): Card[] {
    const values = hand.split(" ");
    return values.map(parseCard);
}

export function parseHands(hands: string): Card[][] {
    const splitHands = [hands.substr(0, 14), hands.substr(15, 14)];
    return splitHands.map(parseHand);
}

export function sortHand(hand: Card[]): Card[] {
    return hand.sort((a, b) => a.value - b.value);
}

export function isStraight(hand: Card[]): boolean {
    const sorted = sortHand(hand).map(card => card.value);

    for (let i = 1; i < sorted.length; i++) {
        if (sorted[i - 1] + 1 !== sorted[i]) {
            return false;
        }
    }

    return true;
}

export function isFlush(hand: Card[]): boolean {
    for (let i = 0; i < hand.length; i++) {
        if (hand[0].suit !== hand[i].suit) {
            return false;
        }
    }

    return true;
}

export function getMatches(hand: Card[]): {value: number, count: number}[] {
    const matches: {[v: string]: number} = {};

    hand.forEach((card) => {
        const value = matches[card.value];
        matches[card.value] = value ? value + 1 : 1;
    });

    const cardsWithMatches = Object.keys(matches).filter(key => matches[key] > 1);
    return cardsWithMatches.map(key => ({value: parseInt(key), count: matches[key]}));
}

export function determineRanking(hand: Card[]): HandComparison {
    const handIsFlush = isFlush(hand);
    const handIsStraight = isStraight(hand);
    const sortedHand = sortHand(hand);
    const highCard = sortedHand[4];

    if (handIsStraight) {
        if (handIsFlush) {
            if (highCard.value === ACE_VALUE) {
                return new NoContestComparison();
            } else {
                return new DeterminingCard(HandRanking.StraitFlush, highCard.value);
            }
        }
        return new DeterminingCard(HandRanking.Straight, highCard.value);
    }

    if (handIsFlush) {
        return new DeterminingCard(HandRanking.Flush, highCard.value);
    }

    const matches = getMatches(hand);

    if (matches.length > 0) {
        if (matches.length === 1) {
            const [{count, value}] = matches;
            if (count === 2) {
                return new SupportingCard(HandRanking.OnePair, value, rankSupportingCards(sortedHand, value));
            } else if (count === 3) {
                return new DeterminingCard(HandRanking.ThreeOfAKind, value);
            } else if (count === 4) {
                return new DeterminingCard(HandRanking.FourOfAKind, value);
            }

        } else if (matches.length === 2) {
            const [matchOne, matchTwo] = matches;
            if (matchOne.count === matchTwo.count) {
                return new SupportingCard(HandRanking.TwoPair, matchOne.value, rankSupportingCards(sortedHand, matchOne.value, matchTwo.value));
            } else {
                const groupOfThreeValue = matchOne.count === 3 ? matchOne.value : matchTwo.value;
                return new DeterminingCard(HandRanking.FullHouse, groupOfThreeValue);
            }
        }
    } else {
        return new SupportingCard(HandRanking.HighCard, highCard.value, rankSupportingCards(sortedHand, highCard.value));
    }

    throw new Error(`Couldn't determine hand ranking ${hand}`);
}

export function rankSupportingCards(hand: Card[], cardToRemove: number, otherCardToRemove: number = 0): number[] {
    return hand
        .filter(card => card.value !== cardToRemove && card.value !== otherCardToRemove)
        .map(card => card.value)
        .reverse();
}

function doesPlayerOneHaveHighestSupportingCard(playerOneSupportingCards: number[], playerTwoSupportingCards: number[]): boolean {
    for (let i = 0; i < playerOneSupportingCards.length; i++) {
        const player1Card = playerOneSupportingCards[i];
        const player2Card = playerTwoSupportingCards[i];

        if (player1Card !== player2Card) {
            return player1Card > player2Card;
        }
    }

    return false;
}

export function doesPlayerOneWin(player1: Card[], player2: Card[]): boolean {
    const player1Rank = determineRanking(player1);
    const player2Rank = determineRanking(player2);

    if (player1Rank.rank === player2Rank.rank) {
        if (player1Rank instanceof DeterminingCard && player2Rank instanceof DeterminingCard) {
            return player1Rank.determiningCard > player2Rank.determiningCard;
        }

        if (player1Rank instanceof SupportingCard && player2Rank instanceof SupportingCard) {
            if (player1Rank.determiningCard !== player2Rank.determiningCard) {
                return player1Rank.determiningCard > player2Rank.determiningCard;
            }

            return doesPlayerOneHaveHighestSupportingCard(player1Rank.supportingCards, player2Rank.supportingCards);
        }
    } else {
        return player1Rank.rank > player2Rank.rank;
    }

    throw new Error(`TIE: ${player1} vs ${player2}`);
}

export function run() {
    const problemLabel = "54. Poker hands";
    const url = "https://projecteuler.net/problem=54";

    runSolution(problemLabel, url, () => {
        const file = readFileSync(POKER_FILE_PATH, "utf-8");
        const hands = file.split("\n").map(parseHands);

        const winningHandsCount = hands
            .map(([a, b]) => doesPlayerOneWin(a, b))
            .filter(didWin => didWin)
            .length;

        console.log(`Answer: ${winningHandsCount}`);
    });
}
