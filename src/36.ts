import runSolution from "./util";

export function isPalindromeRedux(value: string): boolean {
    for (let i = 0; i < value.length / 2; i++) {
        if (value[i] != value[value.length - 1 - i]) {
            return false;
        }
    }

    return true;
}

export function isNumberPalindromeInBase2AndBase10(value: number): boolean {
    const isBase10Palindrome = isPalindromeRedux(value.toString());
    const isBase2Palindrome = isPalindromeRedux(value.toString(2));

    return isBase2Palindrome && isBase10Palindrome;
}

function sumOfDoubleBasePalindromes() {
    let sum = 0;

    for(let i = 0; i < 1000000; i++) {
        sum += isNumberPalindromeInBase2AndBase10(i) ? i : 0;
    }

    return sum;
}

export function run() {
    const problemLabel = "36. Double-base palindromes";
    const url = "https://projecteuler.net/problem=36";

    runSolution(problemLabel, url, () => {
        console.log("Answer: ", sumOfDoubleBasePalindromes());
    });
}
